"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-04-18 22:43
Language: Python 3.7.3
Project: Testing Raspberry Pi version of MicroPython class 'machine.PWM'
"""

from machine import Pin, PWM
from time import sleep

p26 = Pin(26)
led = PWM(p26)

print('Testing get PWM.freq() and PWM.duty()')
print('Frequency: {}Hz'.format(led.freq()))
print('Duty: {}'.format(led.duty()))

sleep(5)
print('=' * 60)

print('Testing set PWM.freq(1)')
led.freq(1)
print('Frequency: {}Hz'.format(led.freq()))
print('Duty: {}'.format(led.duty()))

sleep(5)
print('=' * 60)

print('Testing set PWM.duty(10)')
led.duty(10)
print('Frequency: {}Hz'.format(led.freq()))
print('Duty: {}'.format(led.duty()))

sleep(5)
print('=' * 60)

print('Testing set PWM.duty(800)')
led.duty(800)
print('Frequency: {}Hz'.format(led.freq()))
print('Duty: {}'.format(led.duty()))

sleep(5)
print('=' * 60)

print('Testing set PWM.freq(2) and PWM.duty(10)')
led.freq(2)
led.duty(10)
print('Frequency: {}Hz'.format(led.freq()))
print('Duty: {}'.format(led.duty()))

sleep(5)
print('=' * 60)

print('Testing PWM.deinit()')
led.deinit()
