"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-04-18 16:56
Language: Python 3.7.3
Project: Testing Raspberry Pi version of MicroPython class 'machine.Pin'
"""

from machine import Pin
from time import sleep

led = Pin(26, Pin.OUT)

print('Testing Pin.on(), Pin.off() and get Pin.value()')
for i in range(20):
    print(i)
    led.on()
    print('Led is: {}'.format(led.value()))
    sleep(0.2)
    led.off()
    print('Led is: {}'.format(led.value()))
    sleep(0.2)

print('=' * 60)

print('Testing Pin.value()')
for i in range(20):
    print(i)
    led.value(not(led.value()))
    print('Led is: {}'.format(led.value()))
    sleep(0.2)

print('=' * 60)

print('Testing Pin.__call__()')
for i in range(20):
    print(i)
    led(not(led()))
    print('Led is: {}'.format(led()))
    sleep(0.2)
