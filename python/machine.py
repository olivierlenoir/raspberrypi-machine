"""
Author: Olivier Lenoir - <olivier.len02@gmail.com>
Created: 2020-04-18 11:26:56
Language: Python 3.7.3
Project: Implement MicroPython class 'machine' for Raspberry Pi
Reference:
    https://pinout.xyz
    https://pypi.org/project/RPi.GPIO/
    https://sourceforge.net/p/raspberry-gpio-python/wiki/BasicUsage/
"""

import RPi.GPIO as GPIO


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)


class Pin(object):

    IN = GPIO.IN
    OUT = GPIO.OUT
    IRQ_FALLING = GPIO.FALLING
    IRQ_RISING = GPIO.RISING
    IRQ_LOW_LEVEL = None
    IRQ_HIGH_LEVEL = None
    OPEN_DRAIN = None
    PULL_DOWN = GPIO.PUD_DOWN
    PULL_UP = GPIO.PUD_UP
    PULL_HOLD = None
    WAKE_HIGH = None
    WAKE_LOW = None

    def __init__(self, id, mode=-1, pull=-1):
        self.id = id
        self.__mode = mode
        self.__pull = pull
        self.init()

    def __call__(self, x=None):
        return self.value(x)

    def init(self):
        if self.__mode == Pin.IN and self.__pull != -1:
            GPIO.setup(self.id, self.__mode, pull_up_down=self.__pull)
        elif self.__mode != -1:
            GPIO.setup(self.id, self.__mode)

    def value(self, x=None):
        if x is None:
            return GPIO.input(self.id)
        elif self.__mode == Pin.OUT:
            GPIO.output(self.id, x)
        return None

    def on(self):
        if self.__mode == Pin.OUT:
            GPIO.output(self.id, GPIO.HIGH)

    def off(self):
        if self.__mode == Pin.OUT:
            GPIO.output(self.id, GPIO.LOW)

    def irq(self, handler=None, trigger=None, wake=None, hard=False):
        self.handler = handler
        self.trigger = trigger
        self.wake = wake
        self.hard = hard
        GPIO.add_event_detect(self.id, self.trigger, callback=self.handler)

    def mode(self, mode=None):
        if mode is None:
            return self.__mode
        else:
            self.__mode = mode
            self.init()
        return None

    def pull(self, pull=None):
        if pull is None:
            return self.__pull
        else:
            self.__pull = pull
            self.init()
        return None


class PWM(object):

    MAX_DUTY = 1023

    def __init__(self, pin, freq=5000, duty=512):
        self.pin = pin
        self.frequency = freq
        self.duty_cycle = 100 * duty / PWM.MAX_DUTY
        self.init()

    def init(self):
        GPIO.setup(self.pin.id, self.pin.OUT)
        self.pwm = GPIO.PWM(self.pin.id, self.frequency)
        self.pwm.start(self.duty_cycle)

    def deinit(self):
        self.pwm.stop()

    def duty(self, duty=None):
        if duty is None:
            return int(self.duty_cycle * PWM.MAX_DUTY / 100)
        else:
            self.duty_cycle = 100 * duty / PWM.MAX_DUTY
            self.pwm.ChangeDutyCycle(self.duty_cycle)
        return None

    def freq(self, freq=None):
        if freq is None:
            return self.frequency
        else:
            self.frequency = freq
            self.pwm.ChangeFrequency(self.frequency)
        return None


class I2C(object):
    pass


class UART(object):
    pass


class SPI(object):
    pass
